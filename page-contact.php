<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();
?>

		<div class="large-4 medium-4 small-12 columns sidebar contact">

			<?php the_content(); ?>

		</div>
		<div class="large-8 medium-8 small-12 columns main">

      <div class="contact-content">
        <a href="https://www.google.com.au/maps/place/45+St+Georges+Terrace/@-31.9558399,115.8590376,17z/data=!4m2!3m1!1s0x2a32bb29f013e06d:0x2707f20ddb50029a" target="_blank"><img class="map" src="<?php echo get_template_directory_uri(); ?>/assets/art/contact-map.png" alt="Map to Fletcher Law" /></a>
      </div>

		</div>

<?php get_footer(); ?>
