<?php
/**
 * @package InstallBase
 * @since 2011
 */
 global $post;
?>

<h1><?php the_title(); ?></h1>

<ul class="services">
	<?php

	// If this page has children, display them
	// otherwise, display the main navigation
	$children = get_posts( array(
	
		'post_type' => 'page',
		'post_parent' => $post->ID

	) );

	if( sizeof( $children ) >= 1 )
		wp_list_pages( array(
		
			'title_li' => '',
			'child_of' => $post->ID
		
		) );
	else
		wp_nav_menu( array(
		
			'menu' => 'Main Navigation',
			'items_wrap' => '%3$s',
			'container' => ''
		
		) );
	
	?>
</ul>