<?php

function init_theme () {

	// Script dependencies. jQuery and jQuery UI/Effects cores by default.
	wp_enqueue_script( 'jquery' );

	// Create new thumbnail size of mugshots
	add_image_size( 'mugshot', 150, 150, false );
	add_image_size( 'testimonial-thumb', 330 );

	// Theme dependencies
	add_theme_support( 'post-thumbnails', array( 'post', 'page', 'person', 'project', 'service' ) );
	register_nav_menus( array(
		'primary-navigation' => __( 'Primary Navigation' )
	) );

	// Initialize AJAX dependencies
	wp_localize_script( 'jquery', 'AJAX', array(
		'url' => admin_url( 'admin-ajax.php' ),
		'template_directory' => get_bloginfo( 'template_directory' )
	) );

	// Register 'person' post type
	$post_type_labels = array(
		'name' => _x('People', 'post type general name'),
		'singular_name' => _x('Person', 'post type singular name'),
		'add_new' => _x('Add New', 'post type add generic label'),
		'add_new_item' => __('Add New Person'),
		'edit_item' => __('Edit Person'),
		'new_item' => __('New Person'),
		'view_item' => __('View Person'),
		'search_items' => __('Search People'),
		'not_found' =>  __('No People found'),
		'not_found_in_trash' => __('No People found in the trash'),
		'parent_item_colon' => ''
	);

	register_post_type( 'person', array(
		'labels' => $post_type_labels,
		'singular_label' => _x( 'Person', 'person singular label' ),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'person' ),
		'query_var' => 'person',
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'menu_position' => 5
		)
	);

	// Register 'service' post type
	$post_type_labels = array(
		'name' => _x('Services', 'post type general name'),
		'singular_name' => _x('Service', 'post type singular name'),
		'add_new' => _x('Add New', 'post type add generic label'),
		'add_new_item' => __('Add New Service'),
		'edit_item' => __('Edit Service'),
		'new_item' => __('New Service'),
		'view_item' => __('View Service'),
		'search_items' => __('Search Services'),
		'not_found' =>  __('No Services found'),
		'not_found_in_trash' => __('No Services found in the trash'),
		'parent_item_colon' => ''
	);

	register_post_type( 'service', array(
		'labels' => $post_type_labels,
		'singular_label' => _x( 'Service', 'service singular label' ),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'service' ),
		'query_var' => 'service',
		'supports' => array( 'title', 'editor', 'service', 'thumbnail' ),
		'menu_position' => 5
		)
	);

	// Register 'project' post type
	$post_type_labels = array(
		'name' => _x('Projects', 'post type general name'),
		'singular_name' => _x('Project', 'post type singular name'),
		'add_new' => _x('Add New', 'post type add generic label'),
		'add_new_item' => __('Add New Project'),
		'edit_item' => __('Edit Project'),
		'new_item' => __('New Project'),
		'view_item' => __('View Project'),
		'search_items' => __('Search Projects'),
		'not_found' =>  __('No Projects found'),
		'not_found_in_trash' => __('No Projects found in the trash'),
		'parent_item_colon' => ''
	);

	register_post_type( 'project', array(
		'labels' => $post_type_labels,
		'singular_label' => _x( 'Project', 'project singular label' ),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'project' ),
		'query_var' => 'project',
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'menu_position' => 5
		)
	);
	
	// Register 'testimonial' post type
	$post_type_labels = array(
		'name' => _x('Testimonials', 'post type general name'),
		'singular_name' => _x('Testimonial', 'post type singular name'),
		'add_new' => _x('Add New', 'post type add generic label'),
		'add_new_item' => __('Add New Testimonial'),
		'edit_item' => __('Edit Testimonial'),
		'new_item' => __('New Testimonial'),
		'view_item' => __('View Testimonial'),
		'search_items' => __('Search Testimonials'),
		'not_found' =>  __('No Testimonials found'),
		'not_found_in_trash' => __('No Testimonials found in the trash'),
		'parent_item_colon' => ''
	);

	register_post_type( 'testimonial', array(
		'labels' => $post_type_labels,
		'singular_label' => _x( 'Testimonial', 'testimonial singular label' ),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'testimonial' ),
		'query_var' => 'testimonial',
		'supports' => array( 'title' ),
		'menu_position' => 5
		)
	);

	// Register 'services' taxonomy
		$taxonomy_labels = array(
			'name' => _x( 'Services', 'taxonomy general name' ),
			'singular_name' => _x( 'Service', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Services' ),
			'all_items' => __( 'All Services' ),
			'parent_item' => __( 'Parent Services' ),
			'parent_item_colon' => __( 'Parent Services' ),
			'edit_item' => __( 'Edit Service' ),
			'update_item' => __( 'Update Service' ),
			'add_new_item' => __( 'Add New Service' ),
			'new_item_name' => __( 'New Service' ),
			'menu_name' => __( 'Service Types' )
	  	);

		register_taxonomy( 'services', 'project', array(
				'hierarchical' => true,
				'labels' => $taxonomy_labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'service-type' )
			)
		);

}
add_action( 'init', 'init_theme' );

function init_admin () {

	// Register meta boxes for 'person' post type
	add_meta_box( 'person-data', 'Person Details', 'metabox_person_details', 'person', 'normal', 'high', NULL );

	// Register meta boxes for 'service' post type
	add_meta_box( 'service-data', 'Service Details', 'metabox_service_details', 'service', 'side', 'default', NULL );
	add_meta_box( 'service-featured-project', 'Featured Project', 'metabox_service_featured_project', 'service', 'side', 'default', NULL );

	// Register meta boxes used for multiple post types
	add_meta_box( 'banner-text', 'Banner Text', 'metabox_banner_text', 'page', 'side', 'default', NULL );
	add_meta_box( 'banner-text', 'Banner Text', 'metabox_banner_text', 'service', 'side', 'default', NULL );

	// Admin hooks
	add_action( 'wp_insert_post', 'csf_wp_insert_post', 10, 2 );

}
add_action( 'admin_init', 'init_admin' );

function admin_head() {

	// Ensure post form can handle file uploads
	?>

	<script>
	jQuery( function() {
		jQuery('#post').attr('enctype','multipart/form-data');
	});
	</script>

	<?php

}
add_action( 'admin_head', 'admin_head' );

function metabox_banner_text( $post ) {

	?>
	<input type="text" name="csf_banner_text" value="<?php echo get_post_meta( $post->ID, 'csf_banner_text', true ) ; ?>" style="width:100%;" />
	<?php

}

function metabox_person_details( $post ) {

	$cv = get_post_meta( $post->ID, '_csf_person_cv', true );

	?>

	<table class="testimonial-client-details">

		<tr>
			<td valign="middle" align="left" width="125"><label for="csf_person_title">Title</label></td>
			<td valign="middle" align="left" width="150"><input type="text" name="csf_person_title" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_title', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>The company title of this person. EG, Principal, Secretary etc</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_academic">Academic</label></td>
			<td valign="middle" align="left"><input type="text" name="csf_person_academic" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_academic', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>Academic achievements of this person. EG, &quot;LLB (Hons), BAppPsych&quot;</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_quote">Quote</label></td>
			<td valign="middle" align="left">
				<textarea name="csf_person_quote"><?php echo get_post_meta( $post->ID, 'csf_person_quote', true ); ?></textarea>
			</td>
			<td valign="middle" align="left"><em>&quot;Quote&quot; text to appear in the quote section. Quotation characters are added automatically.</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_email">Email</label></td>
			<td valign="middle" align="left"><input type="text" name="csf_person_email" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_email', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>Email address of this person</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_phone">Phone #</label></td>
			<td valign="middle" align="left"><input type="text" name="csf_person_phone" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_phone', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>Direct contact phone/mobile number of this person&quot;</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_linkedin">Linked In</label></td>
			<td valign="middle" align="left"><input type="text" name="csf_person_linkedin" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_linkedin', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>Web address to the Linked In profile of this person.</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_twitter">Twitter</label></td>
			<td valign="middle" align="left"><input type="text" name="csf_person_twitter" value="<?php echo esc_attr( get_post_meta( $post->ID, 'csf_person_twitter', true ) ); ?>" /></td>
			<td valign="middle" align="left"><em>Web address to Twitter page of this person</em></td>
		</tr>
		<tr>
			<td valign="middle" align="left"><label for="csf_person_cv">CV</label></td>
			<td valign="middle" align="left">
				<input type="file" name="_csf_person_cv" />
				<?php if( is_numeric( $cv ) && $cv != '' ): ?>
				<br />
				&nbsp;<a href="<?php echo wp_get_attachment_url( $cv ); ?>" target="_blank"><?php echo get_the_title( $cv ); ?></a>
				<?php endif; ?>
			</td>
			<td valign="middle" align="left"><em>CV file to upload for this person</em></td>
		</tr>

	</table>

	<?php
}

function metabox_service_details ( $post ) {

	$current_company_contact = get_post_meta( $post->ID, 'csf_service_company_contact', true );

	?>
	<p>Company Contact</p>

	<?php if( $services = get_posts( array( 'post_type' => 'person', 'posts_per_page' => -1 ) ) ): ?>
	<select name="csf_service_company_contact" style="width:100%;">
		<option value="">None</option>
		<?php foreach( $services as $service ): ?>
		<option value="<?php echo $service->ID; ?>"<?php echo $service->ID == $current_company_contact ? ' selected="selected"' : null; ?>><?php echo $service->post_title; ?></option>
		<?php endforeach; ?>
	</select>
	<?php endif; ?>

	<?php

}

function metabox_service_featured_project ( $post ) {

	$current_project = get_post_meta( $post->ID, 'csf_service_featured_project', true );

	?>
	<p>Featured Project</p>

	<?php if( $projects = get_posts( array( 'post_type' => 'project' ) ) ): ?>
	<select name="csf_service_featured_project" style="width:100%;">
		<option value="">None</option>
		<?php foreach( $projects as $project ): ?>
		<option value="<?php echo $project->ID; ?>"<?php echo $project->ID == $current_project ? ' selected="selected"' : NULL; ?>><?php echo $project->post_title; ?></option>
		<?php endforeach; ?>
	</select>
	<?php endif; ?>

	<?php

}

// Generic handler for saving all custom fields across any post type
// fields prefixed with "csf_" only will be saved
function csf_wp_insert_post ( $post_id, $post ) {

	// If saving a person, save uploaded cv if required
	if( 'person' == $post->post_type ) {

		if( !empty( $_FILES['_csf_person_cv']['tmp_name'] ) )
			if( $attachment_id = media_handle_upload( '_csf_person_cv', $post_id ) )
				update_post_meta( $post_id, '_csf_person_cv', $attachment_id );

	}

	// Generic saves
	foreach( $_POST as $key => $value )
		if( strpos( $key, 'csf_' ) === 0 )
			update_post_meta( $post_id, $key, $value );

}

/*======================================================================================
Theme helper functions
======================================================================================*/
// Basic pagination functionality
function the_pagination () {

	echo get_the_pagination();

}

function get_the_pagination () {

	$list = '';

	// Get raw links and translate to a nicer ul
	if( $pagination = get_paginated_links() ) {

		// Prepare return markup
		$list = '<ul>';

		// Loop through each link and sanitize
		foreach( $pagination as $key => &$link ) {

			// If this is the "current" page, wrap it properly
			if( strpos( $link, 'current' ) >= 1 )
				$list .= '<li class="current_page"><a>' . $link . '</a></li>';
			elseif( strpos( $link, 'next' ) === false && strpos( $link, 'prev' ) === false )
				$list .= '<li>' . $link . '</li>';


		}
		$list .= '</ul>';

	}

	return $list;

}

function get_paginated_links ( $type = 'array' ) {

	global $wp_query;

	// Send back paginated links of $type.
	// Basically a common alias for paginate_links
	$large = 999;

	return paginate_links( array(
		'base' => str_replace( $large, '%#%', esc_url( get_pagenum_link( $large ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'type' => $type,
		'total' => $wp_query->max_num_pages
	) );

}

// Basic breadcrumb functionality
function the_breadcrumbs () {

	echo get_the_breadcrumbs();

}

function get_the_breadcrumbs () {

	global $post, $wp_query;



	// Initialize return <ul> and add homepage by default
	$output  = '<p>';
	$output .= '<a href="' . home_url() . '">Home</a>';

	// Prepend logical parents
	if( is_singular( 'post' ) ) {

		if( $category = array_pop( get_the_terms( $post->ID, 'category' ) ) ) {

			$term = get_term_by( 'id', $category->term_id, 'category' );
			$output .= sprintf( '&nbsp;&nbsp;/&nbsp;&nbsp;<a href="%s">%s</a>', get_term_link( $term ), ucwords( $term->name ) );

		}

	}

	if( is_singular( 'service' ) ) {

		$services = get_page_by_path( 'services' );
		$output .= sprintf( '&nbsp;&nbsp;/&nbsp;&nbsp;<a href="%s">%s</a>', get_permalink( $services->ID ), $services->post_title );

	}

	if( is_singular( 'person' ) ) {

		$people = get_page_by_path( 'people' );
		$output .= sprintf( '&nbsp;&nbsp;/&nbsp;&nbsp;<a href="%s">%s</a>', get_permalink( $people->ID ), $people->post_title );

	}

	if( get_query_var( 'cat' ) != '' ) {

		$category = get_term_by( 'id', get_query_var( 'cat' ), 'category' );
		$output .= sprintf( '&nbsp;&nbsp;/&nbsp;&nbsp;<a href="%s">%s</a>', get_term_link( $category ), ucwords( $category->name ) );

	}
	
	if( is_archive() ) {
		
		$output .= "&nbsp;&nbsp;/&nbsp;&nbsp;";

		if ( is_day() ) { $output .= "Daily archives for " . get_the_date(); }
else if ( is_month() ){ $output .= "Monthly archives for " . get_the_date('F, Y'); }
else if ( is_year() ){ $output .= "Yearly archives for " . get_the_date('Y'); }
else { $output .= "Archives"; }

	}
	
	// Prepare array to store all posts in current tree
	$posts = array();

	if( get_query_var( 'cat' ) == '' && !is_archive() ) {

		$posts = array( $post->post_name => $post );

		if( !is_front_page() ) {

			do {
				$post = wp_get_single_post( $post->post_parent );
				$posts[$post->post_name] = $post;
			}
			while( $post->post_parent );

		}

	}

	foreach( array_reverse( $posts ) as $single )
		$output .= sprintf( '&nbsp;&nbsp;/&nbsp;&nbsp;<a href="%s"%s>%s</a>', get_permalink( $single->ID ), $post->ID == $single->ID ? ' class="current_page"' : NULL, $single->post_title );

	$output .= '</p>';

	return $output;

}

// Template tags for post types

// 'Service' post type
function has_featured_project( $service_id = null ) {

	return '' != get_post_meta( $service_id == null ? get_the_ID() : $service_id, 'csf_service_featured_project', true );

}

function get_featured_project( $service_id = null ) {

	return wp_get_single_post( get_post_meta( $service_id == null ? get_the_ID() : $service_id, 'csf_service_featured_project', true ) );

}

function has_company_contact( $service_id = null ) {

	return '' != get_post_meta( $service_id == null ? get_the_ID() : $service_id, 'csf_service_company_contact', true );

}

function get_company_contact( $service_id = null ) {

	if( $person_id = get_post_meta( $service_id == null ? get_the_ID() : $service_id, 'csf_service_company_contact', true ) )
		return wp_get_single_post( $person_id );

	return false;

}

// 'Person' post type
function get_the_company_title( $person_id ) {

	return get_post_meta( $person_id, 'csf_person_title', true );

}

function the_company_title() {

	echo get_the_company_title( get_the_ID() );

}

function get_the_academic( $person_id ) {

	return get_post_meta( $person_id, 'csf_person_academic', true );

}

function the_academic() {

	echo get_the_academic( get_the_ID() );

}

function get_the_quote( $person_id ) {

	return get_post_meta( $person_id, 'csf_person_quote', true );

}

function the_quote() {

	echo get_the_quote( get_the_ID() );

}

function get_the_person_email( $person_id ) {

	return get_post_meta( $person_id, 'csf_person_email', true );

}

function the_person_email() {

	echo get_the_person_email( get_the_ID() );

}

function get_the_phone( $person_id ) {

	return get_post_meta( $person_id, 'csf_person_phone', true );

}

function the_phone() {

	echo get_the_phone( get_the_ID() );

}

function has_cv( $person_id = null ) {

	if( is_null( $person_id ) )
		$person_id = get_the_ID();

	return '' != get_post_meta( $person_id, '_csf_person_cv', true );

}

function get_cv_id( $person_id = null ) {

	if( is_null( $person_id ) )
		$person_id = get_the_ID();

	return get_post_meta( $person_id, '_csf_person_cv', true );

}

/*======================================================================================
Theme specific options and styles selectable from the WISYWIG Editor (TinyMCE)
======================================================================================*/

// Add styles option in to WISYWIG
function tuts_mce_editor_buttons( $buttons ) {

	array_unshift( $buttons, 'styleselect' );
	return $buttons;

}
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );

// Add options to "Styles" dropdown
function tuts_mce_before_init( $settings ) {

  $style_formats = array(

	  array(
	    'title' => 'First Paragraph',
	    'selector' => 'p,h2',
	    'classes' => 'first'
    ),
    array(
	    'title' => 'First Paragraph (no italic)',
	    'selector' => 'p,h2',
	    'classes' => 'first-no-italic'
    )

  );

  $settings['style_formats'] = json_encode( $style_formats );

  return $settings;

}
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );

/*======================================================================================
WordPress filter piggybacks
======================================================================================*/
function excerpt_more( $more ) {

  global $post;
  return sprintf( '... ', get_permalink( $post->ID ) );

  // Currently no mention of "read more" anymore.. wasn't in the design!

}
add_filter( 'excerpt_more', 'excerpt_more' );
/*======================================================================================
Custom Walker_Nav_Menu class to output menu items as <option> elements. Will only work if items_wrap in wp_nav_menu class is set to <select>%3$s</select>
======================================================================================*/
class Walker_Dropdown_Nav_Menu extends Walker_Nav_Menu {

	public function start_el( &$output, $item, $depth, $args ) {

		global $wp_query;

	  // pad out item title to denote sub-menu levels
	  $item->title = str_repeat( '-', $depth * 4 ) . $item->title;

		// get object data and prepare link accordingly
	  $object_meta 	= get_post_meta( $item->ID, '' );
	  $object_id 		= $object_meta['_menu_item_object_id'][0];
	  $object_type 	= $object_meta['_menu_item_type'][0];
	  $object_value	= $object_meta['_menu_item_object'][0];

	  $link 		= '';
	  $selected = $wp_query->queried_object_id == $object_id;

		switch( $object_type ) {

			case 'post_type':
				$link = get_permalink( $object_id );
				break;

			case 'taxonomy':
				$term = get_term_by( 'id', $object_id, $object_value );
				$link = get_term_link( $term );

			// more cases togo here to support all the different types of nav_menu_item's.

		}

	  // change <li> element to <option> element
	  parent::start_el( $output, $item, $depth, $args );
	  $output = str_replace( '<li', sprintf( '<option value="%s"%s', $link, $selected ? ' selected="selected"' : NULL ), $output );

	}

	// replace closing </li> with the closing option tag
	public function end_el( &$output, $item, $depth ) {

	  $output .= "</option>\n";

	}

}

// Helper functions
function pre ( $data ) {

	echo '<pre>' . print_r( $data, true ) . '</pre>';

}

// WordPress login form styling
function login_logo () {

	?>
    <style type="text/css">
        body.login div#login h1 a {
        	display: block;
        	width: 100%;
          background-image: url( '<?php echo get_bloginfo( 'template_directory' ) ?>/assets/art/login-logo.png' ) !important;
          background-size: auto !important;
        }
    </style>
    <?php
}
add_action( 'login_enqueue_scripts', 'login_logo' );

function login_logo_url () {

    return get_bloginfo( 'url' );

}
add_filter( 'login_headerurl', 'login_logo_url' );

function login_logo_url_title () {

    return 'Clever Starfish CMS';

}
add_filter( 'login_headertitle', 'login_logo_url_title' );

function get_the_testimonial_excerpt(){
$permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $newexcerpt = substr($excerpt, 0, 190);
  if ($newexcerpt != $excerpt) {
  $newexcerpt = $newexcerpt.'...';
  }
  return $newexcerpt;
}

/*** Remove Query String from Static Resources ***/
function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

?>
