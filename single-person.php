<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();

 global $post;
?>

		<div class="large-4 medium-4 small-12 columns sidebar">

			<h1 class="hide-for-small"><?php the_title(); ?></h1>
			<h2 class="show-for-small">People</h2>

			<?php get_sidebar( 'people' ); ?>

		</div>
		<div class="large-8 medium-8 small-12 columns main">

			<div class="row person">
                <?php if( has_post_thumbnail() ): ?>
				<div class="large-4 medium-4 columns hide-for-small">
                    <?php the_post_thumbnail('full'); ?>
				</div>
                <?php endif; ?>
				<div class="<?php echo has_post_thumbnail() ? 'large-8 medium-8' : 'large-12 medium-12'; ?> columns">
					<h2><?php the_title(); ?></h2>

					<div class="social">
						<?php if( $twitter = get_post_meta( get_the_ID(), 'csf_person_twitter', true ) ): ?>
							<a href="<?php echo $twitter; ?>" target="_blank">
								<i class="fa fa-twitter-square"></i>
							</a>
							&nbsp;
						<?php endif; ?>
						<?php if( $linkedin = get_post_meta( get_the_ID(), 'csf_person_linkedin', true ) ): ?>
							<a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
						<?php endif; ?>
					</div>

					<br clear="all" />

					<div class="info">
						<p class="title">
							<?php the_company_title(); ?>
						</p>
						<p class="academic">
							<?php the_academic(); ?>
						</p>
						<p class="contact">
							<a href="mailto:<?php the_person_email(); ?>"><?php the_person_email(); ?></a><br />
							<?php the_phone(); ?>
						</p>
					</div>

					<p class="quote">
						&quot;
						<?php the_quote(); ?>
						&quot;
					</p>

					<?php the_content(); ?>

					<?php if( has_cv() ): ?>
					<p class="cv">
						<a href="<?php echo wp_get_attachment_url( get_cv_id() ); ?>" target="_blank">Download CV</a>
					</p>
					<?php endif; ?>

				</div>
			</div>

		</div>

<?php get_footer(); ?>