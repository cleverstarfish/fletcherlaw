<?php
/**
 * @package InstallBase
 * @since 2011
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-59JXXN7');</script>
<!-- End Google Tag Manager -->

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title><?php wp_title(); ?> </title>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/foundation.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/flexslider.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/responsive.css">

	<?php wp_head(); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>

  <script>
    jQuery(document).foundation();
  </script>
  <!--<script src="assets/js/vendor/custom.modernizr.js"></script>-->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.flexslider.js"></script>

</head>
<body id="<?php echo is_front_page() ? 'home' : 'internal'; ?>" <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-59JXXN7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<!-- Begin Markup -->

	<header>

		<div class="row">
			<div class="large-12 columns">

				<a href="<?php echo home_url(); ?>">
					<img id="logo" src="<?php echo get_template_directory_uri(); ?>/assets/art/logo.png" alt="Fletcher Law" />
				</a>
			</div>

			<div class="small-12 columns mobile-navigation show-for-small-only">
				<div class="select-style">
				  <?php

					wp_nav_menu( array(

					  'menu' => 'Main Navigation',
					  'walker' => new Walker_Dropdown_Nav_Menu(),
					  'items_wrap' => '<select>%3$s</select>',
					  'container' => ''

					) );

					?>
				</div>
			</div>

			<div class="large-12 columns navigation hide-for-small-only">

				<nav>
					<?php

					wp_nav_menu( array(

						'menu' => 'Main Navigation',
						'container' => ''

					) );

					?>
				</nav>

			</div>
		</div>


		<div class="row banner">
			<div class="large-12 columns no-padding">
				<div class="row">
					<div class="large-5 medium-8 columns banner-content right flexslider">
						<ul class="slides">
							<?php $testimonials = new WP_Query('post_type=testimonial&posts_per_page=-1&orderby=rand'); ?>
							<?php if ($testimonials->have_posts()) : while ($testimonials->have_posts()) : $testimonials->the_post(); ?>
							<?php $image = get_field('image'); ?>
							<?php if ($image) { ?>
							<li>
								<p><a href="/testimonials"><img src="<?php echo $image['sizes']['testimonial-thumb']; ?>" /></a></p>
								
							</li>
							<?php } ?>
							<?php endwhile; endif; ?>
							<?php wp_reset_query(); ?>
						</ul>
						<div class="row banner-buttons">
							<div class="large-6 small-6 columns banner-button view-services">
								<a href="<?php echo home_url(); ?>/services"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;View Services</a>
							</div>
							<div class="large-6 small-6 columns banner-button meet-our-team">
								<a href="<?php echo home_url(); ?>/people"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Meet our Team</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php if( get_post_meta( get_the_ID(), '_thumbnail_id', true ) && ( is_singular( 'page' ) || is_singular( 'service' ) ) ): ?>
		<style type="text/css">

			header .row.banner {
				background: transparent url( '<?php echo wp_get_attachment_url( get_post_meta( get_the_ID(), '_thumbnail_id', true ) ); ?>' ) center no-repeat !important;
			}

		</style>
		<?php elseif (is_category() || is_archive()): ?>
		<?php $queried_object = get_queried_object(); 
		$taxonomy = $queried_object->taxonomy;
		$term_id = $queried_object->term_id; ?> 
		
		<?php $image = get_field('image', $taxonomy . '_' . $term_id); ?>
		<?php if ($image) { ?>
		<style type="text/css">
		header .row.banner {
				background: transparent url( '<?php echo $image['url']; ?>' ) center no-repeat !important;
		}
		</style>
		<?php } ?>
		<?php endif; ?>

		<!--<div class="row show-for-small-only mobile-banner">
			<?php if( '' != $banner_text ): ?>
			<p><?php echo strip_tags( $banner_text ); ?></p>
			<?php else: ?>
			<p>We are a local & Australian owned law firm, based in the Perth’s CBD</p>
			<?php endif; ?>
		</div>-->

	</header>

	<!-- Begin Content -->
	<div id="content" class="row">

	<?php if( !is_front_page() ): ?>
	<div class="large-12 columns breadcrumbs">
		<?php the_breadcrumbs(); ?>
	</div>
	<?php endif; ?>