<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();
?>

		<div class="large-8 medium-8 small-12 columns main right">

			<h2 class="show-for-small">
				<?php the_title(); ?>
			</h2>

			<?php the_content(); ?>

			<?php if( has_company_contact() ): $contact = get_company_contact(); ?>
			<div class="company-contact">

				<h3>Company Contact</h3>

				<a href="<?php echo get_permalink( $contact->ID ); ?>">
					<?php

					/*if( has_post_thumbnail( $contact->ID ) )

					echo get_the_post_thumbnail( $contact->ID, 'mugshot', array(

						'class' => 'alignleft contact-image',
						'alt' => $contact->post_title,
						'title' => $contact->post_title

					) );*/
					
					

					?>
					<?php $image = get_field('headshot', $contact->ID ); ?>
					<img src="<?php echo $image['sizes']['mugshot']; ?>" class="alignleft contact-image" />
				</a>
				<a href="<?php echo get_permalink( $contact->ID ); ?>">
				<h4><?php echo $contact->post_title; ?></h4></a>
				<p class="title">
					<?php echo get_post_meta( $contact->ID, 'csf_person_title', true ); ?>
				</p>
				<p class="academic">
					<?php echo get_post_meta( $contact->ID, 'csf_person_academic', true ); ?>
				</p>
				<p class="contact">
					<a href="mailto:<?php echo get_post_meta( $contact->ID, 'csf_person_email', true ); ?>"><?php echo get_post_meta( $contact->ID, 'csf_person_email', true ); ?></a><br />
					<?php echo get_post_meta( $contact->ID, 'csf_person_phone', true ); ?>
				</p>

			</div>
			<?php endif; ?>
		</div>

		<div class="large-4 medium-4 small-12 columns sidebar left">

			<h1 class="hide-for-small"><?php the_title(); ?></h1>
			<?php get_sidebar( 'services' ); ?>

		</div>

<?php get_footer(); ?>