<?php
/**
 * @package InstallBase
 * @since 2011
 */
 
 	$people = array();
 
	// Ensure person post with slug 'paul-fletcher' is always first
 
	if( $paul = get_page_by_path( 'paul-fletcher', OBJECT, 'person' ) )
		$people[] = $paul;
 	
	$people = array_merge( $people, get_posts( array(
	
		'post_type' => 'person',
		'numberposts' => -1,
		'orderby' => 'title',
		'order' => 'asc',
		'exclude' => isset( $people[0] ) ? $people[0]->ID : NULL
	
	) ) );
 
?>
<?php if( !empty( $people ) ): ?>
<ul class="services">
	<?php foreach( $people as $person ): ?>
	<li<?php echo ( get_the_ID() == $person->ID ) ? ' class="current_page_item"' : NULL; ?>>
		<a href="<?php echo get_permalink( $person->ID ); ?>"><?php echo $person->post_title; ?></a>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>