<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();
?>

		<div class="large-4 medium-4 small-12 columns sidebar hide-for-small">
			
			<?php get_sidebar(); ?>
			
		</div>
		<div class="large-8 medium-8 small-12 columns main">
			
			<h2 class="show-for-small"><?php the_title(); ?></h2>
			<?php the_content(); ?>
						
		</div>
		
<?php get_footer(); ?>