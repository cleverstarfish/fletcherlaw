<?php
/**
 * @package InstallBase
 * @since 2011
 */
?>
<?php if( $services = get_posts( array( 'post_type' => 'service', 'numberposts' => -1, 'orderby' => 'title', 'order' => 'asc' ) ) ): ?>
<ul class="services<?php echo !is_singular( 'service' ) ? NULL : ' hide-for-small'; ?>">
	<?php foreach( $services as $service ): ?>
		<li<?php echo $service->ID == get_the_ID() ? ' class="current_page_item"' : NULL; ?>><a href="<?php echo get_permalink( $service->ID ); ?>"><?php echo $service->post_title; ?></a></li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php /* 

<?php if( is_singular( 'service' ) ): ?>

	<?php if( has_featured_project() ): $project = get_featured_project(); ?>
	<p class="quote">
		&quot;<?php echo $project->post_content; ?>&quot;
	</p>
	<div class="example-project">
		<?php if( has_post_thumbnail( $project->ID ) ): ?>
		<p class="image">
			<?php echo get_the_post_thumbnail( $project->ID, 'full' ); ?>
		</p>
		<?php endif; ?>
		<p class="company-name">
			<?php echo $project->post_title; ?>
		</p>
		<p class="services-offered">
			services offered:&nbsp;
			<?php
			
			$links = array();
						
			// Loop through 'services' terms assigned to the featured project and
			// try to find a 'Service' post with a matching slug. If so, display link to the post,
			// otherwise, just display the title of the term with no link.
			if( $terms = get_the_terms( $project->ID, 'services' ) ) {
				
				foreach( $terms as $term ) {

					if( $service = get_page_by_path( $term->slug, OBJECT, 'service' ) )
						$links [] = sprintf( '<a href="%s">%s</a>', get_permalink( $service->ID ), $service->post_title );
					else
						$links [] = $term->name;
					
				}
			
			}
			
			?>
			<?php echo implode( ', ', $links ); ?>
		</p>
	</div>
	<?php endif; ?>
	
	<?php
	
	// Get 3 other projects assigned to the same Services (taxonomy) as the featured project
	
	if( $related = get_posts( array(
	
		'post_type' => 'project',
		'posts_per_page' => 3,
		'services' => strip_tags( get_the_term_list( $project->ID, 'services', null, ',', '' ) )
	
	) ) ):
	
	?>
	<div class="related-projects">
		<h3>Related Projects</h3>
		
		<ul>
			<?php foreach( $related as $project ): ?>
			<li><a href="<?php echo get_permalink( $project->ID ); ?>"><?php echo $project->post_title; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	
	

<?php endif; ?>

 */ ?>