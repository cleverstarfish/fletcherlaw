<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header();
?>

		<div class="large-4 medium-4 small-12 columns sidebar news">

			<h1>Publications</h1>


			<h2>Categories</h2>

			<ul class="categories">
				<?php

				wp_list_categories( array(

					'hide_empty' => 0,
					'show_count' => 1,
					'taxonomy' => 'category',
					'title_li' => ''

				) );

				?>
			</ul>

			<h2 class="hide-for-small">Archives</h2>

			<ul class="hide-for-small">
				<?php

				wp_get_archives( array(

					'type' => 'monthly',
					'show_post_count' => 1

				) );

				?>
			</ul>

		</div>
		<div class="large-8 medium-8 small-12 columns main right">

			<?php if( have_posts() ): $pagination = get_the_pagination(); ?>

			<!-- Posts -->
			<div class="row pagination top">
				<div class="large-12 columns">
					<?php echo $pagination; ?>
				</div>
			</div>

			<?php while( have_posts() ): the_post(); ?>
			<div class="row post">
				<?php if( has_post_thumbnail() ): ?>
				<div class="large-4 medium-4 hide-for-small columns">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
				</div>
				<div class="large-8 medium-8 columns">
				<?php else: ?>
				<div class="large-12 medium-12 columns">
				<?php endif; ?>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
			</div>
			<?php endwhile; ?>

			<div class="row pagination">
				<div class="large-12 columns">
					<?php echo $pagination; ?>
				</div>
			</div>
			<!-- End Posts -->
			<?php endif; ?>

		</div>

<?php get_footer(); ?>
