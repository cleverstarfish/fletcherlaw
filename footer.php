<?php
/**
 * @package WordPress
 * @subpackage DigitalUnionInstallBase
 * @since 2011
 */
?>

	</div>
	<!-- End Content -->

	<div id="subscribe" class="row">
		<?php echo do_shortcode( '[gravityform id="1" name="Subscribe to our e-newsletter" description="false" ajax="true"]' ); ?>
	</div>

	<div id="blog-feed">

		<div class="row">
      <?php if( $news = query_posts( array( 'post_type' => 'post', 'category' => 'news', 'posts_per_page' => 3 ) ) ) if( have_posts() ) while( have_posts() ): the_post(); ?>
      <div class="large-4 medium-4 small-12 columns post">
				<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
				<p class="date">
          <?php echo get_the_date( 'jS F Y' ); ?>
				</p>
				<p><?php echo strip_tags( get_the_excerpt() ); ?><a class="read-more" href="<?php the_permalink(); ?>">read more</a></p>
			</div>
    <?php endwhile; wp_reset_query(); ?>
		</div>

	</div>

	<footer>

		<div class="row">
			<div class="large-12 columns">
				<nav>
					<?php

           wp_nav_menu( array(

             'menu' => 'Main Navigation',
             'container' => ''

           ) );

          ?>
				</nav>
			</div>
		</div>
		
		<div class="row">
			<div class="large-12 columns second-menu">
				<nav>
					<ul>
						<li><a href="/privacy-policy">Privacy Policy</a></li>		
						<li><a href="/terms-of-use">Terms of Use</a></li>			
					</ul>
				</nav>
			</div>
		</div>

		<div class="row">
			<div class="large-12 columns">
				<p class="legal">© <?php echo date('Y'); ?> Fletcher Law. All rights reserved</p>
			<div>
		</div>

		<div class="row">
			<div class="large-12 columns">
				<p><a href="http://www.cleverstarfish.com"><img src="<?php echo get_template_directory_uri(); ?>/assets/art/clever-starfish.png" alt="Clever Starfish" /></a></p>
			<div>
		</div>

	</footer>

	<!-- End Markup -->

</body>
</html>
