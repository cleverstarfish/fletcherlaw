<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();
?>

		<div class="large-4 medium-4 small-12 columns sidebar">
			
			<h2>Services</h2>
			
			<?php get_sidebar( 'services' ); ?>
			
		</div>
		<div class="large-8 medium-8 small-12 columns main">
			
			<?php the_content(); ?>
			
		</div>

<?php get_footer(); ?>