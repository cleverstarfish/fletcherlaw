<?php
/**
 * @package InstallBase
 * @since 2011
 */
 get_header(); the_post();
?>

		<div class="large-4 medium-4 small-12 columns sidebar news">

			<h2>Publications</h2>

			<h3>Categories</h3>

			<ul class="categories">
				<?php

				wp_list_categories( array(

					'hide_empty' => 0,
					'show_count' => 1,
					'taxonomy' => 'category',
					'title_li' => ''

				) );

				?>
			</ul>

			<h3 class="hide-for-small">Archives</h3>

			<ul class="hide-for-small">
				<?php

				wp_get_archives( array(

					'type' => 'monthly',
					'show_post_count' => 1

				) );

				?>
			</ul>

		</div>
		<div class="large-8 medium-8 small-12 columns main right">

			<div class="row post">
				<div class="large-12 columns">
          <h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>

			<div class="row post-navigation">
				<div class="large-6 medium-6 small-6 columns">
					<?php if( $previous = get_adjacent_post( false, '', true ) ): ?>
						<p><a href="<?php echo get_permalink( $previous->ID ); ?>">&laquo;&nbsp;<?php echo $previous->post_title; ?></a></p>
					<?php endif; ?>
				</div>
				<div class="large-6 medium-6 small-6 columns">
					<?php if( $next = get_adjacent_post( false, '', false ) ): ?>
						<p class="right"><a href="<?php echo get_permalink( $next->ID ); ?>"><?php echo $next->post_title; ?>&nbsp;&raquo;</a></p>
					<?php endif; ?>
				</div>
			</div>

		</div>

<?php get_footer(); ?>
