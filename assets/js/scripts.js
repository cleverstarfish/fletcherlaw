/*!
 * Fletcher Law Scripts
 * Date: 2013-07-03T13:30Z
 */

jQuery( function($) {

	// Mobile navigation
	$('.mobile-navigation select').on('change', function(e) {
		window.location.href = this.value;	
	});
	
	// Always vertically align banner text (desktop/tablet only)
	$(window).bind('resize', function(e) {
	
		$('.banner-content p').each( function(e) {

			var paragraph = $(this);	

			if (paragraph.height() < ($('.banner-content').height()-50)) {

				paragraph.css('top', (($('.banner-content').height()-50) - paragraph.height()) / 2);
			}
		});
		
	});
	
	// flush
	$(window).trigger('resize');
	
	$('.flexslider').flexslider({
		start: function(){
			$(window).trigger('resize');
		},
		slideshow: false,
		controlNav: false,
		prevText: '&laquo;',
		nextText: '&raquo;'
	});

});